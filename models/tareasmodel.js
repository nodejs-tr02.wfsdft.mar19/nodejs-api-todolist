const mongoose = require('mongoose');

const TareaSchema = new mongoose.Schema({

		tarea: String,
		proyecto: String

}, { versionKey: false });

const Tarea = mongoose.model('tarea', TareaSchema );
module.exports = Tarea;

