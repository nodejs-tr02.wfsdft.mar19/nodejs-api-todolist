let botonAdd = document.querySelector("#addtarea");
let botonGuardar = document.querySelector("#guardar");
let elem_a_editar = null;

let template = document.querySelector("#items");
let target = document.querySelector("tbody");
botonAdd.addEventListener('click', addTarea);
botonGuardar.addEventListener('click', actualizaTarea);

function actualizaTarea (event) {
  let id = elem_a_editar;
  let tarea = document.querySelector("#tarea").value;
  let url = `http://localhost:3000/tarea/${id}`;

  let data = {};

  fetch(url)
  .then(function(response) {
    return response.json();
  })
  .then(function(myJson) {
    console.log(myJson);
    data = myJson;
    data["tarea"] = tarea;
    console.log(data);
    // El Fetch del post se realiza dentro de este callback, ya que depende de manera asincrona de los datos que nos traigamos.

    fetch(url, {
      method: 'PUT', // or 'PUT'
      body: JSON.stringify(data), // data can be `string` or {object}!
      headers:{
        'Content-Type': 'application/json'
      }
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => console.log('Success:', response));
    borrafilas();
    fetchTareas();
  });
  
  clearForm();
  elem_a_editar = null;

  //let data = {"tarea": tarea};


}

function editarTarea (event) {
  let id = event.target.id;
  console.log("Editamos tarea");
  document.querySelector("#tarea").value = document.querySelector("#elemDiv" + id + " td.tarea").innerHTML; 
  //console.log (kk);
  elem_a_editar = id;
};

function addTarea (event) {
  let tarea = document.querySelector("#tarea").value;
  //console.log(tarea);

  let url = 'http://localhost:3000/tarea';
  let data = {"tarea": tarea, "proyecto": "Sin definir"};

  fetch(url, {
  method: 'POST', // or 'PUT'
  body: JSON.stringify(data), // data can be `string` or {object}!
  headers:{
    'Content-Type': 'application/json'
  }
}).then(res => res.json())
.catch(error => console.error('Error:', error))
.then(response => console.log('Success:', response));

borrafilas();
fetchTareas();
clearForm();

}

const borrafilas = () => {
  let filas = document.querySelectorAll("tbody tr.filtareas");
  filas.forEach(function(elem) {
    elem.remove();
  }
  );
}

const borrarTarea = (event) => {
  console.log("BOrra tarea");
  let id = event.target.id;
  
  // Conexión para realizar el fetch
 let url = `http://localhost:3000/tarea/${id}`;
 console.log(url);
 fetch(url, {
 method: 'DELETE', // or 'PUT'
}).then(res => res.json())
.catch(error => console.error('Error:', error))
.then(response => console.log('Success:', response))
  borrafilas();
  fetchTareas();
}

const pintaTareas = (element,key) => {
  console.log(element.tarea);
  let fila_clonada = template.content.cloneNode(true);

  let tr = fila_clonada.querySelector("tr");
  //tr.id = "elemDiv" + key;
  tr.id = "elemDiv" + element._id;

  let td_tarea = fila_clonada.querySelector("td.tarea");
  td_tarea.innerHTML = element.tarea;
  let td_proyecto = fila_clonada.querySelector("td.proyecto");
  td_proyecto.innerHTML = element.proyecto;

  let boton_edit = fila_clonada.querySelector(".btn-primary");
  boton_edit.id = element._id;
  //boton_edit.id = key;
  boton_edit.addEventListener('click', editarTarea);

  let boton_del = fila_clonada.querySelector(".btn-danger");
  //boton_del.id = key;
  boton_del.id = element._id;
  boton_del.addEventListener('click', borrarTarea);

  target.appendChild(fila_clonada);
}

/*const addTarea = (event) => {

}
*/

const clearForm = () => {
  document.querySelector("#tarea").value = "";
}


const fetchTareas = () => {
  fetch('http://localhost:3000/tarea')
  .then(function(response) {
    return response.json();
  })
  .then(function(myJson) {
    console.log(myJson);

    Object.keys(myJson).forEach((key)=>{
        let element = myJson[key];
        pintaTareas (element,key);
    })
    .catch(error => console.error('Error:', error));
  });
}

window.onload = ( ()=> {
    //fetch("http://localhost:3000/tarea",(body)=>{ 
    //    console.log(body)
    //})

    fetchTareas();

  
})
