const express = require('express');
let bodyParser = require('body-parser');
let mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/todolist');

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error: '));

db.once('open', function() {
    console.log("Conectados");
    // Estamos conectados
});

const Book = require('./models/bookmodel');
const Tarea = require('./models/tareasmodel');


const app = express();

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());


let listaTareas = {
     '1': {
         "tarea"  :  "Proyecto Opentable", 
         "proyecto"   :  "HTML"},
     '2': {
         "tarea"  :  "Crear Base de datos NetFlix", 
        "proyecto"   :  "AppFlix"},
     '3': {
         "tarea"  :  "Autenticación usuario", 
         "proyecto"   :  "AppFlix"},
     '4': {
         "tarea"  :  "Diseño Landing Page Personal", 
         "proyecto"   :  "Landing Page Personal"},
};


app.get('/',(req,res) => {
    res.send("listaTareas")
});

app.listen(3000);

app.get('/tarea',(req,res) => {
    Tarea.find(function (err,tareas) {
        if (err){
            return res.status(500).send({"error": "fallo"})
        }
    console.log (tareas);
    return res.json(tareas)
    })
});

app.get('/tarea/:id',(req,res) => {
    //console.log(req.params.id);
    let id = req.params.id;
    Tarea.findById(id, function (err,tareas) {
        if (err){
            return res.status(500).send({"error": "fallo"})
        }
        console.log (tareas);
        return res.json(tareas)
    })
});

app.post('/tarea',(req,res) => {
    let tarea = req.body;
    const newTarea = new Tarea(tarea);
    newTarea.save((err, storedtareas) => {
        if (err){
            return res.status(500).send({"error": "fallo"})
        }
        return res.json(storedtareas)
    });
    //res.send(listaTareas)
});

app.delete('/tarea/:id',(req,res) => {
    let id = req.params.id;
    Tarea.findByIdAndRemove(id, function (err,tareas) {
        if (err){
            return res.status(500).send({"error": "fallo"})
        }
        console.log (tareas);
        return res.json(tareas)
    })
});

app.put('/tarea/:id',(req,res) => {
    let id = req.params.id;
    let tarea = req.body;
    Tarea.findByIdAndUpdate(id, tarea, function (err,tareas) {
        if (err){
            return res.status(500).send({"error": "fallo"})
        }
        console.log (tareas);
        return res.json(tareas)
    })
});
